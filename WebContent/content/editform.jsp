<!DOCTYPE html>  

<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
<title>Edit Form</title>  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Success</title>
<link href="/StudentEnrollmentWithJDBC/assets/css/bootstrap-united.css" rel="stylesheet" />

</head>  
<body>  
  <%@page import="com.vanna.student.repository.StudentRepository"%>
<%@page import="com.vanna.student.model.Student"%>
  <script src="/StudentEnrollmentWithJDBC/jquery-1.8.3.js">
		
	</script>

	<script src="/StudentEnrollmentWithJDBC/bootstrap/js/bootstrap.js">
		
	</script>

	<div class="navbar navbar-default">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<form class="navbar-form navbar-right">
				<input type="text" class="form-control" placeholder="Search">
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentEnrollmentWithJDBC">Home</a></li>
				<li><a href="content/signup.jsp">Signup</a></li>
				<li class="active"><a href="content/login.jsp">Login</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Explore<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">Contact us</a></li>
						<li class="divider"></li>
						<li><a href="#">Further Actions</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.nav-collapse -->
	</div>

	<!-- 
	<legend>Student Enrollment Login Success</legend>
	 -->
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Student Enrollment Updation</h3>
		</div>
	</div>
	<div></div>
	<div></div>
<%  
String userName=request.getParameter("userName");  
Student u=StudentRepository.getStudentByUserName(userName);
%>   
<form action="edituser.jsp" method="post">   
<table>    
<tr><td>Password:</td><td>  
<input type="password" name="password" value="<%= u.getPassword()%>"/></td></tr>  
<tr><td>Email Address:</td><td>  
<input type="email" name="emailAddress" value="<%= u.getEmailAddress()%>"/></td></tr>  
<tr><td colspan="6"><input type="submit" value="Edit User"/></td></tr>  
</table>  
<a class="btn btn-primary" href="viewStudent.jsp">Go Back?
		</a>
</form>  
  
</body>  
</html>  