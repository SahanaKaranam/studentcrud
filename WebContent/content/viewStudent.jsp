<!DOCTYPE html>  
  
<%@page import="com.vanna.student.model.Student"%>
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
<title>View Student Details</title>  
<link href="/StudentEnrollmentWithJDBC/assets/css/bootstrap-united.css" rel="stylesheet" />
</head>  
<body>  
<script src="/StudentEnrollmentWithJDBC/jquery-1.8.3.js">
		
	</script>

	<script src="/StudentEnrollmentWithJDBC/bootstrap/js/bootstrap.js">
		
	</script>

	<div class="navbar navbar-default">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<form class="navbar-form navbar-right">
				<input type="text" class="form-control" placeholder="Search">
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentEnrollmentWithJDBC">Home</a></li>
				<li><a href="content/signup.jsp">Signup</a></li>
				<li class="active"><a href="content/login.jsp">Login</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Explore<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">Contact us</a></li>
						<li class="divider"></li>
						<li><a href="#">Further Actions</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.nav-collapse -->
	</div>

	<!-- 
	<legend>Student Enrollment Details</legend>
	 -->
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Student Enrollment Details</h3>
		</div>
	</div>
	<div></div>
	<div></div>
	
  
<%@page import="com.vanna.student.repository.StudentRepository,com.vanna.student.model.Student.*,java.util.*"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
  
<h1>Student List</h1>  
  
<%  
List<Student> list=StudentRepository.getAllStudents();  
request.setAttribute("list",list);  
%>  
  
<table border="1" width="90%">  
<tr><th>DATE_OF_BIRTH</th><th>EMAIL_ADDRESSS</th><th>FIRST_NAME</th><th>LAST_NAME</th>  
<th>PASSWORD</th><th>USER_NAME</th><th>Edit</th><th>Delete</th></tr>  
<c:forEach items="${list}" var="u">  
<tr><td>${u.getDateOfBirth()}</td><td>${u.getEmailAddress()}</td><td>${u.getFirstName()}</td>  
<td>${u.getLastName()}</td><td>${u.getPassword()}</td><td>${u.getUserName()}</td>  
<td><a href="editform.jsp?userName=${u.getUserName()}">Edit</a></td>  
<td><a href="deleteuser.jsp?userName=${u.getUserName()}">Delete</a></td></tr>  
</c:forEach>  
</table> 
  <a class="btn btn-primary" href="login.jsp">Login
		as different user?</a>
</body>  
</html>