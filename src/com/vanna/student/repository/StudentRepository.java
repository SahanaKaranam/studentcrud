package com.vanna.student.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.vanna.student.model.Student;
import com.vanna.student.util.DbUtil;

public class StudentRepository {
	private Connection dbConnection;

	public StudentRepository() {
		dbConnection = DbUtil.getConnection();
	}

	public void save(String userName, String password, String firstName, String lastName, String dateOfBirth,
			String emailAddress) {
		try {
			PreparedStatement prepStatement = dbConnection.prepareStatement(
					"insert into student(userName, password, firstName, lastName, dateOfBirth, emailAddress) values (?, ?, ?, ?, ?, ?)");
			prepStatement.setString(1, userName);
			prepStatement.setString(2, password);
			prepStatement.setString(3, firstName);
			prepStatement.setString(4, lastName);
			prepStatement.setDate(5, new java.sql.Date(
					new SimpleDateFormat("MM/dd/yyyy").parse(dateOfBirth.substring(0, 10)).getTime()));
			prepStatement.setString(6, emailAddress);

			prepStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public boolean findByUserName(String userName) {
		try {
			PreparedStatement prepStatement = dbConnection
					.prepareStatement("select count(*) from student where userName = ?");
			prepStatement.setString(1, userName);

			ResultSet result = prepStatement.executeQuery();
			if (result != null) {
				while (result.next()) {
					if (result.getInt(1) == 1) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean findByLogin(String userName, String password) {
		try {
			PreparedStatement prepStatement = dbConnection
					.prepareStatement("select password from student where userName = ?");
			prepStatement.setString(1, userName);

			ResultSet result = prepStatement.executeQuery();
			if (result != null) {
				while (result.next()) {
					if (result.getString(1).equals(password)) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static int delete(Student u) {
		int status = 0;
		try {
			Connection con = DbUtil.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from student where userName=?");
			ps.setString(1, u.getUserName());
			status = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}
	public static int update(Student u) {
		int result = 0;
		try {
			Connection con = DbUtil.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update student set emailAddress=?,password=? where userName=?");
			ps.setString(1, u.getEmailAddress());
			ps.setString(2, u.getPassword());
			ps.setString(3, u.getUserName());
			result = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public static List<Student> getAllStudents() {
		List<Student> student = new ArrayList<Student>();

		try {
			Connection con = DbUtil.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from student");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Student stu = new Student();
				stu.setUserName(rs.getString("userName"));
				stu.setPassword(rs.getString("password"));
				stu.setEmailAddress(rs.getString("emailAddress"));
				stu.setDateOfBirth(rs.getDate("dateOfBirth"));
				stu.setFirstName(rs.getString("firstName"));
				stu.setLastName(rs.getString("lastName"));
				student.add(stu);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return student;
	}

	public static Student getStudentByUserName(String userName) {
		Student stu = null;
		try {
			Connection con = DbUtil.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from student where userName=?");
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stu = new Student();
				stu.setUserName(rs.getString("userName"));
				stu.setPassword(rs.getString("password"));
				stu.setEmailAddress(rs.getString("emailAddress"));
				stu.setDateOfBirth(rs.getDate("dateOfBirth"));
				stu.setFirstName(rs.getString("firstName"));
				stu.setLastName(rs.getString("lastName"));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return stu;
	}
}
